# -*- coding: utf-8 -*-
"""
Created on Mon May 21 12:53:44 2018

@author: pi
"""

import il_can_interface.IL_CAN_Commander as ILComm
import il_can_interface.CAN_Manager as CANman
import time
from enum import Enum


class CuvPos(Enum):
    LOADER = 0
    HOLD_1 = 1
    HOLD_2 = 2
    HOLD_3 = 3
    HOLD_4 = 4
    HOLD_5 = 5
    HOLD_6 = 6
    HOLD_7 = 7
    HOLD_8 = 8
    HOLD_9 = 9
    HOLD_10 = 10
    HOLD_11 = 11
    HOLD_12 = 12
    HOLD_13 = 13
    HOLD_14 = 14
    HOLD_15 = 15
    HOLD_16 = 16
    WASTE = 17
    INC_1 = 18
    INC_2 = 19
    INC_3 = 20
    INC_4 = 21
    ORU_1 = 22
    ORU_2 = 23
    ORU_3 = 24
    ORU_4 = 25

# sudo ifconfig can1 down
# sudo ip link set can1 type can bitrate 200000
# sudo ifconfig can1 up

can_id = 0

if can_id==0:
    ic = CANman.CAN_Manager(channel='can0', bustype='socketcan_native', filters=[])
    cc = ILComm.CAN_Commander(stream=ic)
    ic.input_processing_function = cc.CAPP.process_incoming
    cc.CAPP.trace = ic.trace
    ic.start()

shut = cc.CAPP.CUVETTE_CONTROLLER.COMMANDS

def initialize_shuttle(blind_waste_all=True):
    print("Sending controller reset")
    shut.CTRLR_RESET_ID.send()

    time.sleep(5)
    print("Sending controller reset ACK")
    shut.MASTER_RESET_ACK_ID.send()

    time.sleep(3)

    print("Initializing  loader . . .")

    shut.LDR_INITIALIZE_CMD.send(0)

    busy=True
    while busy:
        print("Waiting for initialization to complete . . .")
        time.sleep(5.0)
        busy = (shut.controller.is_busy())

    print("Loader Initialization complete")

    print("Initializing  shuttle . . .")
    shut.SHUTTLE_INITIALIZE_CMD(0)

    print("Shuttle Initialization complete")
    time.sleep(1)

    if blind_waste_all:
        print("Blind waste all slots start")
        for i in range(2, 26):
            if i != 17:
                shut.SHUTTLE_MOVE_SHUTTLE_CMD(i)
                shut.SHUTTLE_EXTEND_CMD()
                shut.SHUTTLE_GRAB_CMD()
                shut.SHUTTLE_RETRACT_CMD()
                shut.SHUTTLE_MOVE_SHUTTLE_CMD(17)
                shut.SHUTTLE_EXTEND_CMD()
                shut.SHUTTLE_RETRACT_CMD()

        shut.SHUTTLE_MOVE_SHUTTLE_CMD(0)
        print("Blind waste all slots complete")
    else:

        print("Blind waste of ORU 4 ...")
        shut.SHUTTLE_MOVE_SHUTTLE_CMD(25)
        shut.SHUTTLE_EXTEND_CMD()
        shut.SHUTTLE_GRAB_CMD()
        shut.SHUTTLE_RETRACT_CMD()
        shut.SHUTTLE_MOVE_SHUTTLE_CMD(17)
        shut.SHUTTLE_EXTEND_CMD()
        shut.SHUTTLE_RETRACT_CMD()
        shut.SHUTTLE_MOVE_SHUTTLE_CMD(0)

        print("Blind waste of ORU 4 complete")

