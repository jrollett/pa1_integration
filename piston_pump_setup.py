import canopen

from beaconcontrols.submodules.motion import MotorAxisMoveParameters
from beaconcontrols.submodules.pistonpump import PistonPump, default_250ul_piston_pump_move_parameters, \
    default_3ml_piston_pump_move_parameters
from beaconcontrols.hal.config import (DisplacementPumpSettings, PistonTransferParameters,
                                       default_test_pump_transfer_params, default_prep_pump_transfer_params)

move_params = MotorAxisMoveParameters(max_velocity=150, acceleration=2000, slip_threshold=50,
                                      move_current=0.6, hold_current=0.4, hold_delay=500)

sample_pump_node_id = 0xA
reagent_test_pump_node_id = 0xB
reagent_prep_pump_node_id = 0xC

sample_test_pump_settings = DisplacementPumpSettings(node_id=sample_pump_node_id, pump_volume=250, backoff_vol=5,
                                                     name='Sample Test Pump')
reagent_test_pump_settings = DisplacementPumpSettings(node_id=reagent_test_pump_node_id, pump_volume=250, backoff_vol=5,
                                                      name='Reagent Test Pump')
reagent_prep_pump_settings = DisplacementPumpSettings(node_id=reagent_prep_pump_node_id, pump_volume=3000,
                                                      backoff_vol=10, name='Reagent Prep Pump')


# %%
def setup_piston_pump(network: canopen.Network, pump_settings: DisplacementPumpSettings,
                      transfer_parameters: PistonTransferParameters,
                      uL_per_full_step: float = 0.2,  # default for 250 uL pump
                      ):
    return PistonPump(network=network, pump_settings=pump_settings,
                      transfer_parameters=transfer_parameters, uL_per_full_step=uL_per_full_step,
                      debug=False)
