import sys
import canopen
import time

from cuv_setup import shut, initialize_shuttle, CuvPos
import il_can_interface.IL_CAN_Commander as ILComm
import il_can_interface.CAN_Manager as CANman
import time
from enum import Enum






# create a CAN network
network = canopen.Network()
bus = network.connect(channel='can0', bustype='socketcan_native', bitrate=200_000)
network.sync.start(0.01)

shut.LDR_INITIALIZE_CMD.send(0)

